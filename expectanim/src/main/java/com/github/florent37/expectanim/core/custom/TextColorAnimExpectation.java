package com.github.florent37.expectanim.core.custom;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * Created by florentchampigny on 21/02/2017.
 */

public class TextColorAnimExpectation extends CustomAnimExpectation {

    private final int textColor;

    public TextColorAnimExpectation(int textColor) {
        this.textColor = textColor;
    }


    @Override
    public Animator getAnimator(Component viewToMove) {
        if (viewToMove instanceof Text) {

            AnimatorProperty animatorProperty =viewToMove.createAnimatorProperty();
            animatorProperty.setDuration(1500);
            animatorProperty.setCurveType(Animator.CurveType.LINEAR);
            return animatorProperty;

        } else {
            return null;
        }
    }
}
