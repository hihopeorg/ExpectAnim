package com.github.florent37.expectanim.core.anim3d;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.ExpectAnimManager;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian Ringshofer on 17/02/2017.
 */
public class ExpectAnimCameraDistanceManager extends ExpectAnimManager {

    @Nullable
    private Integer mCameraDistance = null;

    public ExpectAnimCameraDistanceManager(List<AnimExpectation> animExpectations, Component viewToMove, ViewCalculator viewCalculator) {
        super(animExpectations, viewToMove, viewCalculator);
    }

    @Override
    public void calculate() {
        for (AnimExpectation expectation : animExpectations) {
            if (expectation instanceof CameraDistanceExpectation) {
                final Integer cameraDistance = ((CameraDistanceExpectation) expectation).getCalculatedCameraDistance(viewToMove);
                if (cameraDistance != null) mCameraDistance = cameraDistance;
            }
        }
    }

    @Override
    public List<Animator> getAnimators() {
        final List<Animator> animations = new ArrayList<>();
        calculate();
        if (mCameraDistance != null) {
            AnimatorProperty animatorProperty =viewToMove.createAnimatorProperty();
            animatorProperty.setCurveType(Animator.CurveType.LINEAR);
            animatorProperty.setDuration(1500);
            animations.add(animatorProperty);
        }

        return animations;
    }

    @Nullable
    public Integer getCameraDistance() {
        return mCameraDistance;
    }

}
