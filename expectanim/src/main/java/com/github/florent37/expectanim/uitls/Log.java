package com.github.florent37.expectanim.uitls;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Log {

    public static void d(String tag, String msg) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.debug(LABEL, msg);
    }

    public static void d(String tag, String msg, Throwable tr) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.debug(LABEL, msg, tr);
    }

    public static void e(String tag, String msg) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.error(LABEL, msg);

    }

    public static void e(String tag, String msg, Throwable tr) {

        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.error(LABEL, msg, tr);
    }


    public static void i(String tag, String msg) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.info(LABEL, msg);
    }


    public static void i(String tag, String msg, Throwable tr) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.info(LABEL, msg, tr);
    }


    public static void v(String tag, String msg) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.fatal(LABEL,msg);
    }

    public static void v(String tag, String msg, Throwable tr) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.fatal(LABEL,msg,tr);
    }


    public static void w(String tag, String msg) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.warn(LABEL,msg);
    }


    public static void w(String tag, String msg, Throwable tr) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.warn(LABEL,msg,tr);
    }

    public static void w(String tag, Throwable tr) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, tag);
        HiLog.warn(LABEL, String.valueOf(tr));
    }
 

}







