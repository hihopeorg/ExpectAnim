package com.github.florent37.expectanim.core.position;


import com.github.florent37.expectanim.uitls.Gravity;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationOutOfScreen extends PositionAnimExpectation {

    private int[] gravities;

    public PositionAnimExpectationOutOfScreen(int[] gravities) {
        this.gravities = gravities;
        setForPositionX(true);
        setForPositionY(true);
    }

    private boolean contains(int gravity) {
        for (int g : gravities) {
            if (g == gravity)
                return true;
        }
        return false;
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        if (contains(Gravity.RIGHT) || contains(Gravity.END)) {
//            if (windowManager == null) {
//                windowManager = (WindowManager) viewToMove.getContext().getSystemService(Context.WINDOW_SERVICE);
//            }
//            return (float)viewToMove.getResourceManager().getDeviceCapability().width;
            return 1F * AttrHelper.vp2px(viewToMove.getResourceManager().getDeviceCapability().width, viewToMove.getContext());
//            return   AttrHelper.vp2px(width, viewToMove.getContext());
//            return   AttrHelper.convertValueToFloat(width+"",0f);
        } else if (contains(Gravity.LEFT) || contains(Gravity.START)) {

//            return -1f * viewToMove.getWidth();
            return -1f * viewToMove.getWidth();
        } else
            return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        if (contains(Gravity.BOTTOM)) {
            return 1f * AttrHelper.vp2px(viewToMove.getResourceManager().getDeviceCapability().height, viewToMove.getContext());
        } else if (contains(Gravity.TOP)) {
            return -1f * viewToMove.getHeight();
        } else
            return null;
    }
}
