package com.github.florent37.expectanim.core.rotation;


import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.ExpectAnimManager;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class ExpectAnimRotationManager extends ExpectAnimManager {

    @Nullable
    private Float rotation = null;

    @Nullable
    private Float rotationX;

    @Nullable
    private Float rotationY;

    public ExpectAnimRotationManager(List<AnimExpectation> animExpectations, Component viewToMove, ViewCalculator viewCalculator) {
        super(animExpectations, viewToMove, viewCalculator);
    }

    @Override
    public void calculate() {
        for (AnimExpectation expectation : animExpectations) {
            if (expectation instanceof RotationExpectation) {
                final Float rotation = ((RotationExpectation) expectation).getCalculatedRotation(viewToMove);
                if (rotation != null) {
                    this.rotation = rotation;
                }

                final Float rotationX = ((RotationExpectation) expectation).getCalculatedRotationX(viewToMove);
                if (rotationX != null) {
                    this.rotationX = rotationX;
                }

                final Float rotationY = ((RotationExpectation) expectation).getCalculatedRotationY(viewToMove);
                if (rotationY != null) {
                    this.rotationY = rotationY;
                }
            }
        }
    }

    @Override
    public List<Animator> getAnimators() {
        final List<Animator> animations = new ArrayList<>();

        calculate();
        AnimatorProperty animatorProperty =viewToMove.createAnimatorProperty();
        if (rotation != null) {
            animations.add(animatorProperty.rotate(rotation));
        }
        if (rotationX != null) {
            animations.add(animatorProperty.rotate(rotationX));
        }
        if (rotationY != null) {
            animations.add(animatorProperty.rotate(rotationY));
        }

        return animations;
    }

    public Float getRotation() {
        return rotation;
    }

    @Nullable
    public Float getRotationX() {
        return rotationX;
    }

    @Nullable
    public Float getRotationY() {
        return rotationY;
    }
}
