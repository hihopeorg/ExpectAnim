package com.github.florent37.expectanim.core.position;

import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.ExpectAnimManager;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class ExpectAnimPositionManager extends ExpectAnimManager {
    private Float positionX = null;
    private Float positionY = null;

    private Float translationX = null;
    private Float translationY = null;
    private Long duration = 1500l;

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public ExpectAnimPositionManager(List<AnimExpectation> animExpectations, Component viewToMove, ViewCalculator viewCalculator) {
        super(animExpectations, viewToMove, viewCalculator);
    }


    public Float getPositionX() {
        if (translationX != null) {
            return viewToMove.getContentPositionX() + translationX;
        } else {
            return positionX;
        }
    }

    public Float getPositionY() {
        if (translationX != null) {
            return viewToMove.getContentPositionY() + translationY;
        } else {
            return positionY;
        }
    }

    public void calculate() {
        for (AnimExpectation animExpectation : animExpectations) {
            if (animExpectation instanceof PositionAnimExpectation) {
                PositionAnimExpectation expectation = (PositionAnimExpectation) animExpectation;

                expectation.setViewCalculator(viewCalculator);

                final Float calculatedValueX = expectation.getCalculatedValueX(viewToMove);
                if (calculatedValueX != null) {
                    if (expectation.isForPositionX()) {
                        positionX = calculatedValueX;
                    }
                    if (expectation.isForTranslationX()) {
                        translationX = calculatedValueX;
                    }
                }

                final Float calculatedValueY = expectation.getCalculatedValueY(viewToMove);
                if (calculatedValueY != null) {
                    if (expectation.isForPositionY()) {
                        positionY = calculatedValueY;
                    }
                    if (expectation.isForTranslationY()) {
                        translationY = calculatedValueY;
                    }
                }
            }
        }
    }

    @Override
    public List<Animator> getAnimators() {
        final List<Animator> animations = new ArrayList<>();

        if (positionX != null) {
            AnimatorProperty animator = viewToMove.createAnimatorProperty();
            animator.moveToX(viewCalculator.finalPositionLeftOfView(viewToMove, true));
            animator.setDuration(duration);
            animator.start();
            animations.add(animator);
        }

        if (positionY != null) {
            AnimatorProperty animator = viewToMove.createAnimatorProperty();
            animator.moveToY(viewCalculator.finalPositionTopOfView(viewToMove, true));
            animator.setDuration(duration);
            animator.start();
            animations.add(animator);
        }
        if (translationX != null) {
            AnimatorProperty animator = viewToMove.createAnimatorProperty();
            animator.moveFromY(viewToMove.getWidth());
            animator.moveToX(translationX);
            animator.setDuration(duration);
            animator.start();
            animations.add(animator);
        }

        if (translationY != null) {
            Float screent = 1f * AttrHelper.vp2px(viewToMove.getResourceManager().getDeviceCapability().height, viewToMove.getContext());
            AnimatorProperty animator = viewToMove.createAnimatorProperty();
            animator.moveFromY(screent);
            animator.moveToY(screent - viewToMove.getHeight() - AttrHelper.vp2px(50, viewToMove.getContext()));
            animator.setDuration(duration);
            animator.start();
            animations.add(animator);
        }

        return animations;
    }
}
