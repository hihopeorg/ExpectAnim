package com.github.florent37.expectanim.core.scale;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */
public class ScaleAnimExpectationSameScaleAs extends ScaleAnimExpectationViewDependant {

    public ScaleAnimExpectationSameScaleAs(Component otherView) {
        super(otherView, null, null);
    }

    @Override
    public Float getCalculatedValueScaleX(Component viewToMove) {
        return otherView.getScaleX();
    }

    @Override
    public Float getCalculatedValueScaleY(Component viewToMove) {
        return otherView.getScaleY();
    }
}
