package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationAlignTop extends PositionAnimationViewDependant {

    public PositionAnimExpectationAlignTop(Component otherView) {
        super(otherView);

        setForPositionY(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        return viewCalculator.finalPositionTopOfView(otherView) + getMargin(viewToMove);
    }
}
