package com.github.florent37.expectanim.animation;

import ohos.agp.animation.Animator;

public abstract class AnimatorListenerAdapter implements Animator.StateChangedListener {
    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {

    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }
}
