package com.github.florent37.expectanim.core.alpha;


import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.animation.AnimatorListenerAdapter;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.ExpectAnimManager;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class ExpectAnimAlphaManager extends ExpectAnimManager {
    private Float alpha = null;

    public ExpectAnimAlphaManager(List<AnimExpectation> animExpectations, Component viewToMove, ViewCalculator viewCalculator) {
        super(animExpectations, viewToMove, viewCalculator);
    }

    @Override
    public void calculate() {
        for (AnimExpectation expectation : animExpectations) {
            if (expectation instanceof AlphaAnimExpectation) {
                final Float alpha = ((AlphaAnimExpectation) expectation).getCalculatedAlpha(viewToMove);
                if (alpha != null) {
                    this.alpha = alpha;
                }
            }
        }
    }

    @Override
    public List<Animator> getAnimators() {
        final List<Animator> animations = new ArrayList<>();
        calculate();

        if (alpha != null) {
            AnimatorProperty animatorProperty = viewToMove.createAnimatorProperty();
            animatorProperty.alpha(alpha);
            animatorProperty.setDuration(1500);
            if (alpha == 0) {
                if (viewToMove.getAlpha() != 0) {
                    animations.add(animatorProperty);
                    animatorProperty.setStateChangedListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onEnd(Animator animator) {
                            viewToMove.setVisibility(Component.INVISIBLE);
                        }
                    });

                }
            } else if (alpha == 1) {
                if (viewToMove.getAlpha() != 1) {
                    animations.add(animatorProperty);
                    animatorProperty.setStateChangedListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onStart(Animator animator) {
                            viewToMove.setVisibility(Component.VISIBLE);
                        }
                    });
                }
            } else {
                animations.add(animatorProperty);
                animatorProperty.setStateChangedListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onStart(Animator animation) {
                        viewToMove.setVisibility(Component.VISIBLE);
                    }
                });
            }

        }

        return animations;
    }
}
