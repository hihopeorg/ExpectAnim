package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationOriginal extends PositionAnimExpectation {

    public PositionAnimExpectationOriginal() {
        setForTranslationX(true);
        setForTranslationY(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        return 0f;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        return 0f;
    }
}
