package com.github.florent37.expectanim.uitls;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Path;
import ohos.agp.render.PathMeasure;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * @ProjectName: MyApp
 * @Package: cn.qqtheme.myapp
 * @ClassName: AnimationBuilder
 * @Description: java类作用描述
 * @Author: liys
 * @CreateDate: 2021/3/15 17:41
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/3/15 17:41
 * @UpdateRemark: 更新说明
 * @Version: 1.0.0
 */
public class AnimationBuilder {

    private Component[] zComponent;
    ViewAnimator viewAnimator;
    List<AnimatorValue> animatorList = new ArrayList<>();
    private int singleInterpolator = -1;
    private boolean nextValueWillBeDp = false;
    private boolean waitForHeight;
    HiLogLabel lable = new HiLogLabel(HiLog.INFO, 0x23D434, "AnimationBuilder");

    public int getSingleInterpolator() {
        return singleInterpolator;
    }


    public AnimationBuilder singleInterpolator(int interpolator) {
        singleInterpolator = interpolator;
        return this;
    }

    public Component getView() {
        return zComponent[0];
    }

    public boolean isWaitForHeight() {
        return waitForHeight;
    }

    public AnimationBuilder(ViewAnimator viewAnimator, Component... components) {
        zComponent = components;
        this.viewAnimator = viewAnimator;
    }

    public AnimationBuilder dp() {
        nextValueWillBeDp = true;
        return this;
    }

    /**
     * Start.
     */
    public ViewAnimator start() {
        viewAnimator.start();
        return viewAnimator;
    }

    protected List<AnimatorValue> createAnimators() {
        return animatorList;
    }


    public AnimationBuilder property(String propertyName, float... values) {
        for (Component coms : zComponent) {
            animatorList.add(AnimatorHelp.getPropertyAnimator(coms, AnimatorHelp.onf(propertyName, values)));
        }
        return this;
    }

    public AnimationBuilder pivot(String name, float... values) {
        for (Component coms : zComponent) {
            animatorList.add(AnimatorHelp.getPropertyAnimator(coms, AnimatorHelp.onf(name, values)));
        }
        return this;
    }

    public AnimationBuilder alpha(float... alpha) {
        return property("alpha", alpha);
    }

    public AnimationBuilder andAnimate(Component views) {
        return viewAnimator.addAnimationBuilder(views);
    }


    public AnimationBuilder scaleX(float... scaleX) {
        return property("scaleX", scaleX);
    }

    public AnimationBuilder scaleY(float... scaleY) {
        return property("scaleY", scaleY);
    }

    public AnimationBuilder scale(float... scale) {
//        for (Component coms : zComponent) {
        scaleX(scale);
        scaleY(scale);
//        }
        return this;
    }


    public AnimationBuilder interpolator(int interpolator) {
        viewAnimator.interpolator(interpolator);
        return this;
    }

    public AnimationBuilder rotation(float... rotation) {
        return property("rotation", rotation);
    }

    public AnimationBuilder translationX(float... x) {
        return property("translationX", x);
    }

    public AnimationBuilder translationY(float... y) {
        return property("translationY", y);
    }

    public AnimationBuilder pivotX(float... pivotX) {
        return pivot("pivotX", pivotX);
    }

    public AnimationBuilder pivotY(float... pivotY) {
        return pivot("pivotY", pivotY);
    }

    public AnimationBuilder transX(float... transX) {
        return pivot("transX", transX);
    }

    public AnimationBuilder transY(float... transY) {
        return pivot("transY", transY);
    }

    public AnimationBuilder duration(long duration) {
        viewAnimator.duration(duration);
        return this;
    }

    public AnimationBuilder startDelay(long startDelay) {
        viewAnimator.startDelay(startDelay);
        return this;
    }

    public AnimationBuilder repeatCount(int repeatCount) {
        viewAnimator.repeatCount(repeatCount);
        return this;
    }

    public AnimationBuilder waitForHeight() {
        waitForHeight = true;
        return this;
    }

    public AnimationBuilder width(float... width) {
        return custom(new AnimationListener.Update() {
            @Override
            public void update(Component view, float value) {
                view.getLayoutConfig().width = (int) value;
                view.postLayout();
            }
        }, width);
    }

    public AnimationBuilder textColor(int... colors) {
        for (Component cons : zComponent) {
            for (int i = 0; i < colors.length; i++) {
                int pos = i;
                if (cons instanceof Text) {
                    Text textView = (Text) cons;
                    AnimatorValue objectAnimator = new AnimatorValue();
                    objectAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                        @Override
                        public void onUpdate(AnimatorValue animatorValue, float v) {
                            textView.setTextColor(new Color(colors[pos]));
                        }
                    });
                    this.animatorList.add(objectAnimator);
                }
            }
        }
        return this;
    }

    public AnimationBuilder backgroundColor(int... colors) {
        for (Component cons : zComponent) {
            for (int i = 0; i < colors.length; i++) {
                int pos = i;

                AnimatorValue objectAnimator = new AnimatorValue();
                objectAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(new RgbColor(colors[pos]));
                        cons.setBackground(shapeElement);
                    }
                });
                this.animatorList.add(objectAnimator);
            }
        }
        return this;
    }

    public AnimationBuilder bounce() {
        return translationY(0, 0, -30, 0, -15, 0, 0);
    }

    public AnimationBuilder bounceIn() {
//        for (Component coms : zComponent) {
        scaleY(0.3f, 1.05f, 0.9f, 1);
        scaleX(0.3f, 1.05f, 0.9f, 1);
        alpha(0, 1, 1, 1);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 0.3f, 1.05f, 0.9f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 0.3f, 1.05f, 0.9f, 1);
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1, 1, 1);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, scaleX, scaleY, alpha));
//        }
        return this;
    }

    public AnimationBuilder shake() {
//        AnimatorValue animatorValue=new AnimatorValue();
//        animatorValue.setDuration(3000);
//        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
//            @Override
//            public void onUpdate(AnimatorValue animatorValue, float v) {
//                System.out.println("AnimationBuilder_ValueUpdateListener_v"+v);
//            }
//        });
//
//        this.animatorList.add(animatorValue);
        interpolator(Animator.CurveType.CYCLE);
        return translationX(0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
//        return this;
    }

    public AnimationBuilder tada() {
//        for (Component coms : zComponent) {
        scaleX(1, 0.9f, 0.9f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1);
        scaleY(1, 0.9f, 0.9f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1);
        rotation(0, -3, -3, 3, -3, 3, -3, 3, -3, 0);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 1, 0.9f, 0.9f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 1, 0.9f, 0.9f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1.1f, 1);
//            AnimatorHelp.PropertyValuesHolder rotation = AnimatorHelp.onf("rotation", 0, -3, -3, 3, -3, 3, -3, 3, -3, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, scaleX, scaleY, rotation));
//        }
        return this;
    }

    public AnimationBuilder wave() {
        for (Component coms : zComponent) {
            float x = (coms.getWidth() - coms.getPaddingLeft() - coms.getPaddingRight()) / 2
                    + coms.getPaddingLeft();
            float y = coms.getHeight() - coms.getPaddingBottom();
//            float[] ro = new float[]{12, -12, 3, -3, 0};
//            AnimatorValue animatorValue = new AnimatorValue();
//            animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
//                @Override
//                public void onUpdate(AnimatorValue animatorValue, float v) {
//
//                    for (int i = 0; i < ro.length; i++) {
//                        if (v <= (i + 1) / ro.length){
//                            coms.setRotation(ro[i] * v);
//                            coms.setPivotX(x * v);
//                            coms.setPivotY(y * v);
//                        }
//                    }
//                }
//            });

            AnimatorHelp.PropertyValuesHolder pivotX = AnimatorHelp.onf("pivotX", x, x, x, x, x);
            AnimatorHelp.PropertyValuesHolder pivotY = AnimatorHelp.onf("pivotY", y, y, y, y, y);
            System.out.println("wave x===="+x+"       wave y==========="+y);
            AnimatorHelp.PropertyValuesHolder rotation = AnimatorHelp.onf("rotation", 12, -12, 3, -3, 0);
            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms,rotation, pivotX, pivotY));
//            this.animatorList.add(animatorValue);
        }
        return this;
    }

    public AnimationBuilder standUp() {
        for (Component coms : zComponent) {
            float x = (coms.getWidth() - coms.getPaddingLeft() - coms.getPaddingRight()) / 2
                    + coms.getPaddingLeft();
            float y = coms.getHeight() - coms.getPaddingBottom();
            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("pivotX", x, x, x, x, x);
            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("pivotY", y, y, y, y, y);
            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("rotation", 55, -30, 15, -15, 0);
            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, scaleX, scaleY));
        }
        return this;
    }

    public AnimationBuilder swing() {
        return rotation(0, 10, -10, 6, -6, 3, -3, 0);
    }

    public AnimationBuilder wobble() {
        for (Component coms : zComponent) {
            float width = coms.getWidth();
            float one = (float) (width / 100.0);
            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("translationX", 0 * one, -25 * one, 20 * one, -15 * one, 10 * one, -5 * one, 0 * one, 0);
            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("rotation", 0, -5, 3, -3, 2, -1, 0);
            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, scaleX, scaleY));
        }
        return this;
    }

    public AnimationBuilder flash() {
        return alpha(1, 0, 1, 0, 1);
    }

    public AnimationBuilder pulse() {
        scaleX(1, 1.1f, 1);
        scaleY(1, 1.1f, 1);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 1, 1.1f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 1, 1.1f, 1);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, scaleX, scaleY));
//        }
        return this;
    }

    public AnimationBuilder zoomIn() {
        alpha(0, 1);
        scaleX(0.45f, 1);
        scaleY(0.45f, 1);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 0.45f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 0.45f, 1);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, scaleX, scaleY));
//        }
        return this;
    }

    public AnimationBuilder zoomOut() {
        alpha(1, 0, 0);
        scaleX(1, 0.3f, 0);
        scaleY(1, 0.3f, 0);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 1, 0, 0);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 1, 0.3f, 0);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 1, 0.3f, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, scaleX, scaleY));
//        }
        return this;
    }

    public AnimationBuilder fall() {
        return rotation(1080, 720, 360, 0);
    }

    public AnimationBuilder thenAnimate(Component views) {
        return viewAnimator.thenAnimate(views);
    }

    public AnimationBuilder newsPaper() {
        alpha(0, 1, 1);
        scaleX(0.1f, 0.5f, 1);
        scaleY(0.1f, 0.5f, 1);
        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1, 1);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 0.1f, 0.5f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 0.1f, 0.5f, 1);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, scaleX, scaleY));
        }
        return this;
    }

    public AnimationBuilder slit() {
        rotation(90, 88, 88, 45, 0);
        alpha(0, 0.4f, 0.8f, 1);
        scaleX(0, 0.5f, 0.9f, 0.9f, 1);
        scaleY(0, 0.5f, 0.9f, 0.9f, 1);
//                    mComponent.setRotationy(rotation[pos]);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 0.4f, 0.8f, 1, 1);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 0, 0.5f, 0.9f, 0.9f, 1, 0);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 0, 0.5f, 0.9f, 0.9f, 1, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, scaleX, scaleY));
//        }
        return this;
    }

    public AnimationBuilder slideLeftIn() {
        alpha(0, 1);
        translationX(-300, 0);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1);
//            AnimatorHelp.PropertyValuesHolder translationX = AnimatorHelp.onf("translationX", -300, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, translationX));
//        }
        return this;
    }

    public AnimationBuilder slideRightIn() {
        alpha(0, 1);
        translationX(300, 0);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1);
//            AnimatorHelp.PropertyValuesHolder translationX = AnimatorHelp.onf("translationX", 300, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, translationX));
//        }
        return this;
    }

    public AnimationBuilder slideTopIn() {
        alpha(0, 1);
        translationY(-300, 0);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1);
//            AnimatorHelp.PropertyValuesHolder translationY = AnimatorHelp.onf("translationY", -300, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, translationY));
//        }
        return this;
    }

    public AnimationBuilder slideBottomIn() {
        alpha(0, 1);
        translationY(300, 0);
        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1);
//            AnimatorHelp.PropertyValuesHolder translationY = AnimatorHelp.onf("translationY", 300, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, translationY));
        }
        return this;
    }

    public AnimationBuilder fadeIn() {
//        for (Component coms : zComponent) {
        alpha(0, 0.25f, 0.5f, 0.75f, 1);

//        }
        return this;
    }

    public AnimationBuilder fadeOut() {
        alpha(1, 0.75f, 0.5f, 0.25f, 0);
//        for (Component coms : zComponent) {
//            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 1, 0.75f, 0.5f, 0.25f, 0);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha));
//        }
        return this;
    }

    public AnimationBuilder rollIn() {
        for (Component coms : zComponent) {
            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 0, 1);
            AnimatorHelp.PropertyValuesHolder translationX = AnimatorHelp.onf("translationX", -(coms.getWidth() - coms.getPaddingLeft() - coms.getPaddingRight()), 0);
            AnimatorHelp.PropertyValuesHolder rotation = AnimatorHelp.onf("rotation", -120, 0);
            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, translationX, rotation));
        }
        return this;
    }

    public AnimationBuilder rollOut() {
        for (Component coms : zComponent) {
            AnimatorHelp.PropertyValuesHolder alpha = AnimatorHelp.onf("alpha", 1, 0);
            AnimatorHelp.PropertyValuesHolder translationX = AnimatorHelp.onf("translationX", 0, coms.getWidth());
            AnimatorHelp.PropertyValuesHolder rotation = AnimatorHelp.onf("rotation", 0, 120);
            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, alpha, translationX, rotation));
        }
        return this;
    }

    public AnimationBuilder rubber() {
//        for (Component coms : zComponent) {
        scaleX(1, 1.25f, 0.75f, 1.15f, 1);
        scaleY(1, 0.75f, 1.25f, 0.85f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleX = AnimatorHelp.onf("scaleX", 1, 1.25f, 0.75f, 1.15f, 1);
//            AnimatorHelp.PropertyValuesHolder scaleY = AnimatorHelp.onf("scaleY", 1, 0.75f, 1.25f, 0.85f, 1);
//            this.animatorList.add(AnimatorHelp.getPropertyAnimator(coms, scaleX, scaleY));
//        }
        return this;
    }

    public AnimationBuilder path(Path path) {
        if (path == null) {
            return this;
        }
        final PathMeasure pathMeasure = new PathMeasure(path, false);
        return custom(new AnimationListener.Update() {
            @Override
            public void update(Component view, float value) {
                HiLog.debug(lable, "custom-update" + value);
                if (view == null) {
                    return;
                }
                float[] currentPosition = new float[2];
                pathMeasure.getPosTan(value, currentPosition, null);
                final float x = currentPosition[0];
                final float y = currentPosition[1];
                view.setTranslationX(x);
                view.setTranslationY(y);
                HiLog.info(lable, "path: value=" + value + ", x=" + x + ", y=" + y);
            }
        }, 0, pathMeasure.getLength());
    }

    protected AnimationBuilder add(AnimatorValue animator) {
        this.animatorList.add(animator);
        return this;
    }

    public AnimationBuilder custom(final AnimationListener.Update update, float... values) {
        for (Component coms : zComponent) {
            AnimatorValue valueAnimator = new AnimatorValue();
            if (update != null) {
                valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        //noinspection unchecked
                        update.update(coms, v);
                    }
                });
            }
            add(valueAnimator);
        }
        return this;
    }

    public AnimationBuilder onStop(AnimationListener.Stop stopListener) {
        viewAnimator.onStop(stopListener);
        return this;
    }
}
