package com.github.florent37.expectanim.uitls;


import ohos.agp.utils.LayoutAlignment;


public class Gravity {

    public static final int TOP = LayoutAlignment.TOP;
    public  static  final  int BOTTOM=LayoutAlignment.BOTTOM;
    public  static  final  int LEFT=LayoutAlignment.LEFT;
    public  static  final  int RIGHT=LayoutAlignment.RIGHT;
    public static  final  int END=LayoutAlignment.END;
    public  static  final  int START=LayoutAlignment.START;
    public  static  final int CENTER=LayoutAlignment.CENTER;
    public  static  final  int CENTER_HORIZONTAL= LayoutAlignment.HORIZONTAL_CENTER;
    public  static  final  int CENTER_VERTICAL=LayoutAlignment.VERTICAL_CENTER;



}
