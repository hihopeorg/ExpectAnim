package com.github.florent37.expectanim.uitls;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: MyApp
 * @Package: cn.qqtheme.myapp
 * @ClassName: ViewAnimator
 * @Description: java类作用描述
 * @Author: liys
 * @CreateDate: 2021/3/15 17:38
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/3/15 17:38
 * @UpdateRemark: 更新说明
 * @Version: 1.0.0
 */
public class ViewAnimator {
    private static final long DEFAULT_DURATION = 3000L;
    private ViewAnimator prev = null;
    private ViewAnimator next = null;
    private AnimatorGroup animatorSet;

    private int interpolator = -1;
    private int runSet = 1;
    private int repeatCount = 1;
    private AnimationListener.Start startListener;
    private AnimationListener.Stop stopListener;
    private Component waitForThisViewHeight = null;
    List<AnimationBuilder> animationList = new ArrayList<>();
    private long duration = 3000;
    private long startDelay = 0;

    public AnimationBuilder thenAnimate(Component views) {
        ViewAnimator nextViewAnimator = new ViewAnimator();
        this.next = nextViewAnimator;
        nextViewAnimator.prev = this;
        return nextViewAnimator.addAnimationBuilder(views);
    }


    public static AnimationBuilder animate(Component... components) {

        ViewAnimator viewAnimator = new ViewAnimator();
        return viewAnimator.addAnimationBuilder(components);
    }

    public AnimationBuilder addAnimationBuilder(Component... views) {
        AnimationBuilder animationBuilder = new AnimationBuilder(this, views);
        animationList.add(animationBuilder);
        return animationBuilder;
    }

    protected AnimatorGroup createAnimatorSet() {
        List<Animator> animators = new ArrayList<>();
        for (AnimationBuilder animationBuilder : animationList) {
            List<AnimatorValue> list = animationBuilder.createAnimators();
            if (animationBuilder.getSingleInterpolator() != -1) {
                for (AnimatorValue animator : list) {
                    animator.setCurveType(animationBuilder.getSingleInterpolator());
                }
            }
            animators.addAll(list);
        }
        for (AnimationBuilder animationBuilder : animationList) {
            if (animationBuilder.isWaitForHeight()) {
                waitForThisViewHeight = animationBuilder.getView();
                break;
            }
        }
        AnimatorGroup animatorSet = new AnimatorGroup();
        Animator[] animators1 = new Animator[animators.size()];
        for (int i = 0; i < animators.size(); i++) {
            animators1[i] = animators.get(i);
        }
        animatorSet.runParallel(animators1);
        animatorSet.setDuration(duration);
        animatorSet.setDelay(startDelay);
        if (repeatCount != 1)
            animatorSet.setLoopedCount(repeatCount);
        animatorSet.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (startListener != null) startListener.onStart();
            }

            @Override
            public void onStop(Animator animator) {
                if (stopListener != null) stopListener.onStop();
                if (next != null) {
                    next.prev = null;
                    next.start();
                }
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        return animatorSet;
    }

    /**
     * -1 or INFINITE will repeat forever
     */
    public ViewAnimator repeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
        return this;
    }

    public void cancel() {
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        if (next != null) {
            next.cancel();
            next = null;
        }
    }

    public ViewAnimator duration(long duration) {
        this.duration = duration;
        return this;
    }

    public ViewAnimator startDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    /**
     * see https://github.com/cimi-chen/EaseInterpolator
     */
    public ViewAnimator interpolator(int interpolator) {
        this.interpolator = interpolator;
        return this;
    }

    /**
     * see https://github.com/cimi-chen/EaseInterpolator
     */
    public ViewAnimator runSet(int interpolator) {
        this.runSet = interpolator;
        return this;
    }

    public ViewAnimator onStop(AnimationListener.Stop stopListener) {
        this.stopListener = stopListener;
        return this;
    }

    public void start() {
        if (prev != null) {
            prev.start();
        } else {
            animatorSet = createAnimatorSet();
            animatorSet.start();
        }
    }
}
