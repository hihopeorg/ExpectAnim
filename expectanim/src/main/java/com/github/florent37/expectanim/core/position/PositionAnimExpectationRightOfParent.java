package com.github.florent37.expectanim.core.position;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentParent;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationRightOfParent extends PositionAnimExpectation {

    public PositionAnimExpectationRightOfParent() {
        setForPositionX(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
       final ComponentParent viewParent= viewToMove.getComponentParent();
        if((viewParent instanceof Component)){
            final Component parentView = (Component)viewParent;
            return parentView.getWidth() - getMargin(viewToMove) - viewCalculator.finalWidthOfView(viewToMove);
        }
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        return null;
    }
}
