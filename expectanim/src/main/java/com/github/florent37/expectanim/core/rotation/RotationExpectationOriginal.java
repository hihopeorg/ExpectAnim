package com.github.florent37.expectanim.core.rotation;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */
public class RotationExpectationOriginal extends RotationExpectation {

    @Override
    public Float getCalculatedRotation(Component viewToMove) {
        return 0f;
    }

    @Override
    public Float getCalculatedRotationX(Component viewToMove) {
        return 0f;
    }

    @Override
    public Float getCalculatedRotationY(Component viewToMove) {
        return 0f;
    }
}
