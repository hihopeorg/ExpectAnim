package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentParent;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationCenterBetweenViewAndParent extends PositionAnimationViewDependant {

    private boolean horizontal;
    private boolean vertical;
    private boolean toBeOnRight;
    private boolean toBeOnBottom;

    public PositionAnimExpectationCenterBetweenViewAndParent(Component otherView, boolean horizontal, boolean vertical, boolean toBeOnRight, boolean toBeOnBottom) {
        super(otherView);
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.toBeOnRight = toBeOnRight;
        this.toBeOnBottom = toBeOnBottom;

        setForPositionY(true);
        setForPositionX(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        if (horizontal) {
            final ComponentParent viewParent = otherView.getComponentParent();
            if ((viewParent instanceof Component) && horizontal) {
                final Component parentView = (Component) viewParent;
                final float centerOfOtherView = viewCalculator.finalCenterXOfView(otherView);
                if (toBeOnRight) {
                    final float parentWidth = parentView.getWidth();
                    return (parentWidth + centerOfOtherView) / 2f - viewToMove.getWidth() / 2f;
                } else {
                    return centerOfOtherView / 2f - viewToMove.getWidth() / 2f;
                }
            }
        }
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        if (vertical) {
            final  ComponentParent viewParent=viewToMove.getComponentParent();
            if ((viewParent instanceof Component) && vertical) {
                final Component parentView = (Component) viewParent;
                final float centerOfOtherView = viewCalculator.finalCenterYOfView(otherView);
                if (toBeOnBottom) {
                    final float parentHeight = parentView.getHeight();
                    return parentHeight + centerOfOtherView / 2f - viewToMove.getHeight() / 2f;
                } else {
                    return centerOfOtherView / 2f - viewToMove.getHeight() / 2f;
                }

            }
        }
        return null;
    }
}
