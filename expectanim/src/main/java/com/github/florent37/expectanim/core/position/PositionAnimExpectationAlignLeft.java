package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationAlignLeft extends PositionAnimationViewDependant {

    public PositionAnimExpectationAlignLeft(Component otherView) {
        super(otherView);

        setForPositionX(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        return viewCalculator.finalPositionLeftOfView(otherView) + getMargin(viewToMove) ;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        return null;
    }
}
