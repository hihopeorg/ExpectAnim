package com.github.florent37.expectanim.core.custom;



import com.github.florent37.expectanim.core.AnimExpectation;
import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 21/02/2017.
 */

public abstract class CustomAnimExpectation extends AnimExpectation {
    public abstract Animator getAnimator(Component viewToMove);
}
