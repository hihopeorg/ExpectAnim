package com.github.florent37.expectanim.uitls;



import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Retention(SOURCE)
@Target({ANNOTATION_TYPE})
public @interface IntDef {

    int[] value() default {};


    boolean flag() default false;


    boolean open() default false;
}
