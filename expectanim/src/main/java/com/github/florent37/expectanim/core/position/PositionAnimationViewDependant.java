package com.github.florent37.expectanim.core.position;


import com.github.florent37.expectanim.ViewCalculator;
import ohos.agp.components.Component;

import java.util.List;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public abstract class PositionAnimationViewDependant extends PositionAnimExpectation {

    protected Component otherView;

    public PositionAnimationViewDependant(Component otherView) {
        this.otherView = otherView;
    }

    @Override
    public List<Component> getViewsDependencies() {
        final List<Component> viewsDependencies = super.getViewsDependencies();
        viewsDependencies.add(otherView);
        return viewsDependencies;
    }

    public void setViewCalculator(ViewCalculator viewCalculator) {
        this.viewCalculator = viewCalculator;
    }

}
