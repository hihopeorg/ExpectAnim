package com.github.florent37.expectanim.core.scale;


import ohos.agp.components.Component;

import java.util.List;

/**
 * Created by florentchampigny on 20/02/2017.
 */
public abstract class ScaleAnimExpectationViewDependant extends ScaleAnimExpectation {

    protected final Component otherView;

    public ScaleAnimExpectationViewDependant(Component otherView, Integer gravityHorizontal, Integer gravityVertical) {
        super(gravityHorizontal, gravityVertical);
        this.otherView = otherView;
    }

    @Override
    public List<Component> getViewsDependencies() {
        final List<Component> viewsDependencies = super.getViewsDependencies();
        viewsDependencies.add(otherView);
        return viewsDependencies;
    }

    public abstract Float getCalculatedValueScaleX(Component viewToMove);

    public abstract Float getCalculatedValueScaleY(Component viewToMove);
}
