package com.github.florent37.expectanim.core.position;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentParent;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationCenterInParent extends PositionAnimExpectation {

    public boolean horizontal;
    public boolean vertical;

    public PositionAnimExpectationCenterInParent(boolean horizontal, boolean vertical) {
        this.horizontal = horizontal;
        this.vertical = vertical;

        setForPositionX(true);
        setForPositionY(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        final ComponentParent viewParent = viewToMove.getComponentParent();
        if ((viewParent instanceof Component) && horizontal) {
            final Component parentView = (Component) viewParent;
            return parentView.getWidth() / 2f - viewToMove.getWidth() / 2f;
        }
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {

        final ComponentParent viewParent= viewToMove.getComponentParent();
        if ((viewParent instanceof Component) && vertical) {
            final Component parentView = (Component) viewParent;
            return parentView.getHeight() / 2f - viewToMove.getHeight() / 2f;
        }
        return null;
    }
}
