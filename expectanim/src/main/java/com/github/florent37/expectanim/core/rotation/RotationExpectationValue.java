package com.github.florent37.expectanim.core.rotation;



import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */
public class RotationExpectationValue extends RotationExpectation {

    private final float rotation;

    public RotationExpectationValue(float rotation) {
        this.rotation = rotation;
    }

    @Override
    public Float getCalculatedRotation(Component viewToMove) {
        return rotation;
    }

    @Override
    public Float getCalculatedRotationX(Component viewToMove) {
        return null;
    }

    @Override
    public Float getCalculatedRotationY(Component viewToMove) {
        return null;
    }
}
