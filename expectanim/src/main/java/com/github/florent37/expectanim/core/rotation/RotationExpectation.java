package com.github.florent37.expectanim.core.rotation;


import com.github.florent37.expectanim.core.AnimExpectation;
import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 18/02/2017.
 */
public abstract class RotationExpectation extends AnimExpectation {

    public abstract Float getCalculatedRotation(Component viewToMove);
    public abstract Float getCalculatedRotationX(Component viewToMove);
    public abstract Float getCalculatedRotationY(Component viewToMove);
}
