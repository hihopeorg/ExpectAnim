package com.github.florent37.expectanim.uitls;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.text.DecimalFormat;


public class AnimatorHelp {
    private final static HiLogLabel lable = new HiLogLabel(HiLog.INFO, 0x5444324, "AnimatorHelp");
    final static DecimalFormat df = new DecimalFormat("0.00");
    static  String currentName="";

    public static class Keyframe {
        float progress;
        float value;

        public Keyframe(float progress, float value) {
            this.progress = progress;
            this.value = value;
        }

        public static Keyframe ofFloat(float progress, float value) {
            return new Keyframe(progress, value);
        }
    }

    public static class PropertyValuesHolder {
        String property;
        Keyframe[] frames;

        public PropertyValuesHolder(String property, Keyframe... frames) {
            this.property = property;
            this.frames = frames;
        }

        public static PropertyValuesHolder ofKeyframe(String property, Keyframe... frames) {
            return new PropertyValuesHolder(property, frames);
        }
    }

    public static PropertyValuesHolder onf(String property, float... values) {

        Keyframe[] keyframes = new Keyframe[values.length];
        for (int i = 0; i < values.length; i++) {
            float pro = (float) i / values.length;
            keyframes[i] = Keyframe.ofFloat(Float.parseFloat(df.format(pro)), values[i]);
        }
        return new PropertyValuesHolder(property, keyframes);
    }

    public static AnimatorValue getPropertyAnimator(Component component, String property, Keyframe... frames) {
        return getPropertyAnimator(component, new PropertyValuesHolder(property, frames));
    }

    public static AnimatorValue getPropertyAnimator(final Component component, PropertyValuesHolder... holders) {
        System.out.println("getPropertyAnimator--holders.length" + holders.length);
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setDuration(3000);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float p = Float.parseFloat(df.format(v));
                System.out.println("getPropertyAnimator--float  v" + p);
                for (PropertyValuesHolder holder : holders) {
                    System.out.println("holders.frames.length" + holder.frames.length);
                    for (int i = 0; i < holder.frames.length; i++) {

                        Keyframe keyframe = null;
                        float value;
                        float value2;
                        float progress=holder.frames[i].progress;//动画进度

                        float secend_progress=i>0?holder.frames[i-1].progress:0;//动画进度

                        float Incremental_p= i>0?(v-progress)/(progress-secend_progress):0;//增量进度
                        System.out.println("增量进度======"+Incremental_p);
                        if (v >= holder.frames[holder.frames.length - 1].progress) {
                            if(i==holder.frames.length - 1) {
                                keyframe = holder.frames[i];
                                value = keyframe.value;
                                value2 = holder.frames[i - 1].value;
                                currentName=holder.property;
                                switch (holder.property) {
                                    case "scaleX":
                                        component.setScaleX(value2+(value-value2)* Incremental_p);
                                        break;
                                    case "scaleY":
                                        component.setScaleY(value2+(value-value2)* Incremental_p);
                                        break;
                                    case "alpha":
                                        component.setAlpha(value2+(value-value2)* Incremental_p);
                                        break;
                                    case "translationX":
                                        component.setTranslationX(value2+(value-value2)* Incremental_p);
                                        break;
                                    case "translationY":
                                        component.setTranslationY(value2+(value-value2)* Incremental_p);
                                        break;
                                    case "rotation":
                                        component.setRotation(value2+(value-value2)* Incremental_p);
                                        break;
                                    case "pivotX":
                                        component.setPivotX(value);
                                        break;
                                    case "pivotY":
                                        component.setPivotY(value);
                                        break;
                                    case "percent":



                                        break;
                                }
                            }
                        } else
                            if
                        (i<holder.frames.length-2&&v >= holder.frames[i].progress && v < holder.frames[i + 1].progress) {
                            if (holder.frames.length - 1 >= i) {
                                keyframe = holder.frames[i];
                            } else {
                                keyframe = holder.frames[i - 1];
                            }
                            value = keyframe.value;
                            value2 = holder.frames[i +1].value;
                            switch (holder.property) {
                                case "scaleX":
                                    System.out.println(String.format("scaleX=======value=%s---;value2=%s;---Incremental_p=%s---",value,value2,Incremental_p));
                                    component.setScaleX(value+(value2-value)* Incremental_p);

                                    break;
                                case "scaleY":
                                    component.setScaleY(value+(value2-value)* Incremental_p);
                                    break;
                                case "alpha":
                                    component.setAlpha(value+(value2-value)* Incremental_p);
                                    break;
                                case "translationX":
//                                    System.out.println("translationX i!=frames.length-1-------------" + value);
                                    component.setTranslationX(value+(value2-value)* Incremental_p);
                                    break;
                                case "translationY":
                                    component.setTranslationY(value+(value2-value)* Incremental_p);
                                    break;
                                case "rotation":
                                    component.setRotation(value+(value2-value)* Incremental_p);
                                    break;
                                case "pivotX":
                                    component.setPivotX(value);
                                    break;
                                case "pivotY":
                                    component.setPivotY(value);
                                    break;

                            }
                        }
                    }
                }
            }
        });
        return animatorValue;
    }

}
