package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentParent;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationBottomOfParent extends PositionAnimExpectation {

    public PositionAnimExpectationBottomOfParent() {
        setForPositionY(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        final ComponentParent viewParent = viewToMove.getComponentParent();
        if ((viewParent instanceof Component)) {
            final Component parentView = (Component) viewParent;
            return parentView.getHeight() - getMargin(viewToMove) - viewCalculator.finalHeightOfView(viewToMove);
        }
        return null;
    }
}
