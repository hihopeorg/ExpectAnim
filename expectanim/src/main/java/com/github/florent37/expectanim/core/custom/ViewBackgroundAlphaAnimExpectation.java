package com.github.florent37.expectanim.core.custom;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;

/**
 * Created by florentchampigny on 21/02/2017.
 */

public class ViewBackgroundAlphaAnimExpectation extends CustomAnimExpectation {

    private final float alpha;

    public ViewBackgroundAlphaAnimExpectation(float alpha) {
        this.alpha = alpha;
    }


    @Override
    public Animator getAnimator(Component viewToMove) {
        if (viewToMove != null) {
            AnimatorValue animatorValue = new AnimatorValue();
            animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                ShapeElement shapeElement = new ShapeElement();

                @Override
                public void onUpdate(AnimatorValue animatorValue, float value) {
                    int v = viewToMove.getBackgroundElement().getAlpha();
                    viewToMove.setAlpha((int) (255 * v));
                }
            });
            return animatorValue;
        } else {


            return null;
        }
    }
}
