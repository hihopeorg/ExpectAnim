package com.github.florent37.expectanim.core;



import com.github.florent37.expectanim.ViewCalculator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

import java.util.List;

/**
 * Created by florentchampigny on 21/02/2017.
 */

public abstract class ExpectAnimManager {

    protected final List<AnimExpectation> animExpectations;
    protected final Component viewToMove;
    protected final ViewCalculator viewCalculator;

    public ExpectAnimManager(List<AnimExpectation> animExpectations, Component viewToMove, ViewCalculator viewCalculator) {
        this.animExpectations = animExpectations;
        this.viewToMove = viewToMove;
        this.viewCalculator = viewCalculator;
    }

    public abstract void calculate();

    public abstract List<Animator> getAnimators();

}
