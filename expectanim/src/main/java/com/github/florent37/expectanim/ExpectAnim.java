package com.github.florent37.expectanim;


import com.github.florent37.expectanim.animation.AnimatorListenerAdapter;
import com.github.florent37.expectanim.animation.Interpolator;
import com.github.florent37.expectanim.listener.AnimationEndListener;
import com.github.florent37.expectanim.listener.AnimationStartListener;
import com.github.florent37.expectanim.uitls.Log;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class ExpectAnim {

    private static final long DEFAULT_DURATION = 300l;

    private List<ViewExpectation> expectationList;
    private Component anyView;

    private long startDelay = 5;

    private List<Component> viewToMove;
    private ViewCalculator viewCalculator;

    //    private AnimatorSet animatorSet;
    private AnimatorGroup animatorSet;

    private List<AnimationEndListener> endListeners = new ArrayList<>();
    private List<AnimationStartListener> startListeners = new ArrayList<>();
    private AtomicBoolean isPlaying = new AtomicBoolean(false);

    private List<Animator> preAnimatorList = new ArrayList<>();


    @Nullable
    private Interpolator interpolator;
    private Long duration = DEFAULT_DURATION;

    public ExpectAnim() {
        this.expectationList = new ArrayList<>();
        this.viewToMove = new ArrayList<>();
        this.viewCalculator = new ViewCalculator();
    }

    public ViewExpectation expect(Component view) {
        this.anyView = view;
        final ViewExpectation viewExpectation = new ViewExpectation(this, view);
        expectationList.add(viewExpectation);
        return viewExpectation;
    }

    private ExpectAnim calculate() {
        if (animatorSet == null) {
            animatorSet = new AnimatorGroup();

//            if (interpolator != null) {
//                animatorSet.setInterpolator(interpolator);
//
//            }
            animatorSet.setCurveType(Animator.CurveType.LINEAR);
            animatorSet.setDuration(duration);

            final List<Animator> animatorList = new ArrayList<>();

            final List<ViewExpectation> expectationsToCalculate = new ArrayList<>();

            //"ViewDependencies" = récupérer toutes les vues des "Expectations"
            for (ViewExpectation viewExpectation : expectationList) {
                viewExpectation.calculateDependencies();
                viewToMove.add(viewExpectation.getViewToMove());
                expectationsToCalculate.add(viewExpectation);

                viewCalculator.setExpectationForView(viewExpectation.getViewToMove(), viewExpectation);
            }

            while (!expectationsToCalculate.isEmpty()) {
                //pour chaque expectation dans "Expectations"
                final Iterator<ViewExpectation> iterator = expectationsToCalculate.iterator();
                while (iterator.hasNext()) {
                    final ViewExpectation viewExpectation = iterator.next();

                    //regarder si une de ces dépendance est dans "ViewDependencies"
                    if (!hasDependency(viewExpectation)) {
                        //si non
                        viewExpectation.calculate(viewCalculator);
                        animatorList.addAll(viewExpectation.getAnimations());

                        final Component view = viewExpectation.getViewToMove();
                        viewToMove.remove(view);
                        viewCalculator.wasCalculated(viewExpectation);

                        iterator.remove();
                    } else {
                        //si oui, attendre le prochain tour
                    }
                }
            }

            animatorSet.setStateChangedListener(new AnimatorListenerAdapter() {
                @Override
                public void onEnd(Animator animator) {
                    super.onEnd(animator);
                    isPlaying.set(false);
                    notifyListenerEnd();
                }

                @Override
                public void onStart(Animator animator) {
                    super.onStart(animator);
                    isPlaying.set(true);
                    notifyListenerStart();
                }

            });

//            animatorSet.playTogether(animatorList);
            preAnimatorList = animatorList;
            Animator[] animators = new Animator[animatorList.size()];
            for (int i = 0; i < animatorList.size(); i++) {
                animators[i] = animatorList.get(i);
            }
            animatorSet.runParallel(animators);

        }
        return this;
    }

    private void notifyListenerStart() {
        for (AnimationStartListener startListener : startListeners) {
            if (startListener != null) {
                startListener.onAnimationStart(ExpectAnim.this);
            }
        }
    }

    private void notifyListenerEnd() {
        for (AnimationEndListener endListener : endListeners) {
            if (endListener != null) {
                endListener.onAnimationEnd(ExpectAnim.this);
            }
        }
    }

    public ExpectAnim setStartDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    public ExpectAnim start() {
//        executeAfterDraw(anyView, new Runnable() {
//            @Override
//            public void run() {
//              calculate();
//               animatorSet.start();
//            }
//        });
        new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
            calculate();
            animatorSet.start();

        });
        return this;
    }

    private boolean hasDependency(ViewExpectation viewExpectation) {
        final List<Component> dependencies = viewExpectation.getDependencies();
        if (!dependencies.isEmpty()) {
            for (Component view : viewToMove) {
                if (dependencies.contains(view)) {
                    return true;
                }
            }
        }
        return false;
    }

    boolean isReset = true;

    public void setPercent(float percent) {
        calculate();
        if (animatorSet != null) {
            if (percent == 1 && isReset) {
                for (int i = 0; i < preAnimatorList.size(); i++) {
                    if(preAnimatorList.get(i) instanceof AnimatorValue){
                        AnimatorValue animator = (AnimatorValue) preAnimatorList.get(i);
                        animator.release();
                    }else if(preAnimatorList.get(i) instanceof AnimatorProperty ){
                        AnimatorProperty animator = (AnimatorProperty) preAnimatorList.get(i);
                        animator.setDuration((long) (percent * animator.getDuration()));

                        animator.reset();
                    }

                    Log.d("TAG", "------------");
                }
                isReset = false;
            } else if (percent == 0) {
                for (int i = 0; i < preAnimatorList.size(); i++) {
                    AnimatorProperty animator = (AnimatorProperty) preAnimatorList.get(i);
                    animator.setDuration((long) (percent * animator.getDuration()));
                    animator.start();
                    Log.d("TAG", "------------");
                }
                isReset = true;
            }


        }

    }

    public void setPercent(float percent, boolean isReset) {
        calculate();
        if (isReset) {

            for (int i = 0; i < preAnimatorList.size(); i++) {
                AnimatorProperty animator = (AnimatorProperty) preAnimatorList.get(i);
                animator.setDuration((long) (percent * animator.getDuration()));
                animator.reset();
            }

        } else {
            for (int i = 0; i < preAnimatorList.size(); i++) {
                AnimatorProperty animator = (AnimatorProperty) preAnimatorList.get(i);
                animator.setDuration((long) (percent * animator.getDuration()));
                animator.start();
                Log.d("TAG", "------------");
            }
        }
    }

    public boolean isPlaying() {
        return isPlaying.get();
    }

    public void setNow() {
        executeAfterDraw(anyView, () -> setPercent(1f));
    }

    public void executeAfterDraw(final Component view, final Runnable runnable) {
        new EventHandler(EventRunner.getMainEventRunner()).postTask(runnable, Math.max(5, startDelay));


    }

    public void reset() {
        if (animatorSet != null) {
            AnimatorProperty[] animatorProperties = new AnimatorProperty[preAnimatorList.size()];
            for (int i = 0; i < preAnimatorList.size(); i++) {
                if (preAnimatorList.get(i) instanceof AnimatorProperty) {
                    AnimatorProperty animator = (AnimatorProperty) preAnimatorList.get(i);
                    animator.setDelay(1000);
                    animator.reset();
                    animatorProperties[i] = animator;
                }

            }
            animatorSet.runParallel(animatorProperties);
        }


    }

    public ExpectAnim setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;
        return this;
    }

    public ExpectAnim setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public ExpectAnim addEndListener(AnimationEndListener listener) {
        this.endListeners.add(listener);
        return this;
    }

    public ExpectAnim addStartListener(AnimationStartListener listener) {
        this.startListeners.add(listener);
        return this;
    }

    private void concatWith(final ExpectAnim otherAnim) {
        addEndListener(new AnimationEndListener() {
            @Override
            public void onAnimationEnd(ExpectAnim expectAnim) {
                otherAnim.start();
            }
        });
    }

    public static ExpectAnim concat(ExpectAnim expectAnim, ExpectAnim... expectAnims) {
        if (expectAnims.length > 0) {
            expectAnim.concatWith(expectAnims[0]);
            for (int i = 0; i < expectAnims.length - 1; i++) {
                expectAnims[i].concatWith(expectAnims[i + 1]);
            }
        }
        return expectAnim;
    }

    public  long getDuration() {
        return duration;
    }
}
