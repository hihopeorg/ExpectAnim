package com.github.florent37.expectanim.animation;

public interface Interpolator {

    float getInterpolation(float input);
}
