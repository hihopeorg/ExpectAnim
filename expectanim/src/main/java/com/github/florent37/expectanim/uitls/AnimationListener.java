package com.github.florent37.expectanim.uitls;


import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 22/12/2015.
 */
public class AnimationListener {

    private AnimationListener(){}

    public interface Start{
        void onStart();
    }

    public interface Stop{
        void onStop();
    }

    public interface Update<V extends Component>{
        void update(V view, float value);
    }

}
