package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationTopOfParent extends PositionAnimExpectation {

    public PositionAnimExpectationTopOfParent() {
        setForPositionY(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        return getMargin(viewToMove);
    }
}
