package com.github.florent37.expectanim.core.position;



import com.github.florent37.expectanim.core.AnimExpectation;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.app.Context;
import org.jetbrains.annotations.Nullable;


/**
 * Created by florentchampigny on 17/02/2017.
 */

public abstract class PositionAnimExpectation extends AnimExpectation {
    public abstract Float getCalculatedValueX(Component viewToMove);
    public abstract Float getCalculatedValueY(Component viewToMove);

    private boolean isForPositionY;
    private boolean isForPositionX;
    private boolean isForTranslationX;
    private boolean isForTranslationY;
    private float margin;

    @Nullable
    private Integer marginRes;

    @Nullable
    private Float marginDp;

    public boolean isForPositionY() {
        return isForPositionY;
    }

    public boolean isForPositionX() {
        return isForPositionX;
    }

    public boolean isForTranslationX() {
        return isForTranslationX;
    }

    public boolean isForTranslationY() {
        return isForTranslationY;
    }

    protected void setForPositionY(boolean forPositionY) {
        isForPositionY = forPositionY;
    }

    protected void setForPositionX(boolean forPositionX) {
        isForPositionX = forPositionX;
    }

    protected void setForTranslationX(boolean forTranslationX) {
        isForTranslationX = forTranslationX;
    }

    protected void setForTranslationY(boolean forTranslationY) {
        isForTranslationY = forTranslationY;
    }

    public float getMargin(Component view) {
        if(marginRes != null){

            margin=  AttrHelper.vp2px(marginRes,view.getContext());


        } else if(marginDp != null){
            margin = dpToPx(view.getContext(), marginDp);
        }
        return margin;
    }

    public static int  dpToPx(Context context, float dp){
        return AttrHelper.vp2px(dp,context);
    }

    public PositionAnimExpectation withMargin(float margin){
        this.margin = margin;
        return this;
    }

    public PositionAnimExpectation withMarginDimen( int marginRes){
        this.marginRes = marginRes;
        return this;
    }

    public PositionAnimExpectation withMarginDp(float marginDp){
        this.marginDp = marginDp;
        return this;
    }

}
