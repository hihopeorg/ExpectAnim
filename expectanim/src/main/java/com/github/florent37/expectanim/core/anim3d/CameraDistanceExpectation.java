package com.github.florent37.expectanim.core.anim3d;



import com.github.florent37.expectanim.core.AnimExpectation;
import ohos.agp.components.Component;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Christian Ringshofer on 17/02/2017.
 */
public abstract class CameraDistanceExpectation extends AnimExpectation {

    @Nullable
    public abstract Integer getCalculatedCameraDistance(Component viewToMove);

}
