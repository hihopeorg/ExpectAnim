package com.github.florent37.expectanim.core.position;

import ohos.agp.components.Component;

/**
 * Created by florentchampigny on 17/02/2017.
 */

public class PositionAnimExpectationAlignBottom extends PositionAnimationViewDependant {

    public PositionAnimExpectationAlignBottom(Component otherView) {
        super(otherView);

        setForPositionY(true);
    }

    @Override
    public Float getCalculatedValueX(Component viewToMove) {
        return null;
    }

    @Override
    public Float getCalculatedValueY(Component viewToMove) {
        return viewCalculator.finalPositionBottomOfView(otherView) - getMargin(viewToMove) - viewToMove.getHeight();
    }
}
