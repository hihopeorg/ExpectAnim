

#### version 1.0.1:

- 对sample 功能进行优化（修复了follow 弹闪问题）
- 对rotation 功能进行优化

#### version 1.0.0:
   已实现功能：
 * 动画支持aplha，位置，旋转和缩放功能
   

  未实现功能：

 * 动画翻转功能（flip 和 concat ）

  

  

  