# ExpectAnim
本项目是基于开源项目 ExpectAnim 进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/florent37/ExpectAnim ）追踪到原项目版本

#### 项目介绍

* 项目名称 ：链式属性动画
* 所属系列：ohos的第三方组件适配移植
* 功能：动画支持aplha，位置，旋转和缩放功能。
* 项目移植状态：完成
* 调用差异：无
* 项目作者和维护人：hihope
* 联系方式：hihope@hoperun.com
* 原项目Doc地址：https://github.com/florent37/ExpectAnim
* 原项目基线版本：1.0.7  shal:a5f6fc93858048f412bc038411d9b43e8f6bb6dd
* 编程语言：java
* 外部库依赖：无

#### 效果展示图

![](./screenshot/expectanim.gif)

#### 安装教程

方法(一)
1. 编译ExpectAnim的har包。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
方法(二)

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.github.florent37.ohos:expectanim:1.0.1'
}

```

#### 使用说明

（1）滑动缩放效果代码
```
        this.expectAnimMove = new ExpectAnim()
                .expect(avatar)
                .toBe(
                        topOfParent().withMarginDp(20),
                        leftOfParent().withMarginDp(20),
                        scale(0.5f, 0.5f)
                )

                .expect(username)
                .toBe(
                        toRightOf(avatar).withMarginDp(16),
                        sameCenterVerticalAs(avatar),
                        alpha(0.5f)
                )

                .expect(follow)
                .toBe(
                        rightOfParent().withMarginDp(20),
                        sameCenterVerticalAs(avatar)
                )

                .expect(backbground)
                .toBe(
                        height(290).withGravity(Gravity.LEFT, Gravity.TOP)
                )

                .toAnimation();
```
（2）位置移动效果代码示例
```
 message.setClickedListener(component -> {

             expectAnimMove.start();

         });

         follow.setClickedListener(component -> {
                    initFollow();
                }
        );
        intBottom();

        new ExpectAnim()
                .expect(bottomLayout)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )
                .expect(content)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )
                .toAnimation()
                .setNow();

        this.expectAnimMove = new ExpectAnim()

                .expect(avatar)
                .toBe(
                        bottomOfParent().withMarginDp(36),
                        leftOfParent().withMarginDp(16),
                        width(40).toDp().keepRatio()
                )

                .expect(name)
                .toBe(
                        toRightOf(avatar).withMarginDp(16),
                        sameCenterVerticalAs(avatar),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(subname)
                .toBe(
                        toRightOf(name).withMarginDp(5),
                        sameCenterVerticalAs(name),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(follow)
                .toBe(
                        rightOfParent().withMarginDp(4),
                        bottomOfParent().withMarginDp(12),
                        toHaveBackgroundAlpha(1f)
                )

                .expect(message)
                .toBe(
                        aboveOf(follow).withMarginDp(4),
                        rightOfParent().withMarginDp(4),
                        toHaveBackgroundAlpha(1f)
                )

                .expect(bottomLayout)
                .toBe(
                        atItsOriginalPosition(),
                        visible()
                )

                .expect(content)
                .toBe(
                        atItsOriginalPosition(),
                        visible()
                )

                .toAnimation()
                .setDuration(1500);
```
#### 版本迭代
v1.0.1

#### 版权和许可信息
```
Copyright 2017 florent37, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```