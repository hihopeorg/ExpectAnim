package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.uitls.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExpectAnimRotationAbilityTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }
    @Test
    public void onStart() {
        ExpectAnimRotationAbility expectAnimRotationAbility = EventHelper.startAbility(ExpectAnimRotationAbility.class);
        DirectionalLayout directionalLayout = (DirectionalLayout) expectAnimRotationAbility.findComponentById(ResourceTable.Id_content);
        Text text1 = (Text) expectAnimRotationAbility.findComponentById(ResourceTable.Id_text1);
        int[] startLocation = text1.getLocationOnScreen();
        sleep(2000);
        EventHelper.triggerClickEvent(expectAnimRotationAbility, directionalLayout);
        sleep(2000);
        int[] endLocation = text1.getLocationOnScreen();
        assertTrue("Whether the starting and ending positions are equal", startLocation[1] != endLocation[1]);
        sAbilityDelegator.stopAbility(expectAnimRotationAbility);
    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}