package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignBottom;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignLeft;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionAnimExpectationAlignLeftTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getCalculatedValueX() {

        Text text=new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator=new ViewCalculator();
        PositionAnimExpectationAlignLeft positionAnimExpectationAlignLeft   =new PositionAnimExpectationAlignLeft(text);
        positionAnimExpectationAlignLeft.setViewCalculator(viewCalculator);
        Float aFloat  =positionAnimExpectationAlignLeft.getCalculatedValueX(text);
        assertTrue("getCalculatedValueX() of value is not null", aFloat!=null);
    }
}