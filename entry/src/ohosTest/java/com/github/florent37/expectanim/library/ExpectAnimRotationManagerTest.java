package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.Expectations;
import com.github.florent37.expectanim.core.rotation.ExpectAnimRotationManager;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExpectAnimRotationManagerTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getAnimators() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.rotated(30));
        Text text = new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator = new ViewCalculator();
        ExpectAnimRotationManager expectAnimRotationManager= new ExpectAnimRotationManager(animExpectations,text,viewCalculator);
        List<Animator> animators= expectAnimRotationManager.getAnimators();
        assertTrue("animators is greater than zero", animators.size()>0);

    }
}