package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignRight;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignTop;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionAnimExpectationAlignTopTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getCalculatedValueY() {

        Text text=new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator=new ViewCalculator();
        PositionAnimExpectationAlignTop positionAnimExpectationAlignTop   =new  PositionAnimExpectationAlignTop(text);
        positionAnimExpectationAlignTop.setViewCalculator(viewCalculator);
        Float aFloat  =positionAnimExpectationAlignTop.getCalculatedValueY(text);
        assertTrue("getCalculatedValueY() of value is not null", aFloat!=null);


    }
}