package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.uitls.EventHelper;
import ohos.agp.components.NestedScrollView;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExpectAnimScrollAbilityTest {
    long shortTime = 2000;
    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }
    @Test
    public void onStart() {
        ExpectAnimScrollAbility expectAnimScrollAbility = EventHelper.startAbility(ExpectAnimScrollAbility.class);
        sleep(shortTime);
        Text text = (Text) expectAnimScrollAbility.findComponentById(ResourceTable.Id_follow);
        NestedScrollView scrollView = (NestedScrollView) expectAnimScrollAbility.findComponentById(ResourceTable.Id_scrollview);
        int[] startLocation = text.getLocationOnScreen();
        EventHelper.inputSwipe(expectAnimScrollAbility, scrollView, 0, 1000, 0, 100, 5000);
        sleep(5000);
        int[] endLocation = text.getLocationOnScreen();
        assertTrue("Whether the starting and ending positions are equal", startLocation[0] != endLocation[0] && startLocation[1] != endLocation[1]);

    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}