package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.Expectations;
import com.github.florent37.expectanim.core.alpha.AlphaAnimExpectation;
import com.github.florent37.expectanim.core.alpha.ExpectAnimAlphaManager;
import com.github.florent37.expectanim.core.anim3d.ExpectAnimCameraDistanceManager;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.github.florent37.expectanim.core.Expectations.visible;
import static org.junit.Assert.*;

public class ExpectAnimCameraDistanceManagerTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Test
    public void getAnimators() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.withCameraDistance(10));
        Text text = new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator = new ViewCalculator();
        ExpectAnimCameraDistanceManager cameraDistanceManager = new ExpectAnimCameraDistanceManager(animExpectations, text, viewCalculator);
        List<Animator> animators= cameraDistanceManager.getAnimators();

        assertTrue("ExpectAnimCameraDistanceManagerTest of calculate is not run", animators.size() > 0);


    }

    @Test
    public void getCameraDistance() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.withCameraDistance(10));
        Text text = new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator = new ViewCalculator();
        ExpectAnimCameraDistanceManager cameraDistanceManager = new ExpectAnimCameraDistanceManager(animExpectations, text, viewCalculator);
        cameraDistanceManager.calculate();
        Integer integer = cameraDistanceManager.getCameraDistance();
        assertTrue("ExpectAnimCameraDistanceManagerTest of getCameraDistance is not value >0", integer > 0);
    }
}