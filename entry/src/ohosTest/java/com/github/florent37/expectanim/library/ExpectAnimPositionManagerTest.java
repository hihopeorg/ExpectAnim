package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.Expectations;
import com.github.florent37.expectanim.core.position.ExpectAnimPositionManager;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExpectAnimPositionManagerTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();


    @Test
    public void getPositionY() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.alignTop(new Text(sAbilityDelegator.getAppContext())));
        Text text = new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator = new ViewCalculator();
        ExpectAnimPositionManager expectAnimPositionManager = new ExpectAnimPositionManager(animExpectations, text, viewCalculator);
        expectAnimPositionManager.calculate();
        Float positionX = expectAnimPositionManager.getPositionY();
        assertTrue("getPositionY is not null", positionX != null);
    }


    @Test
    public void getAnimators() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.alignTop(new Text(sAbilityDelegator.getAppContext())));
        Text text = new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator = new ViewCalculator();
        ExpectAnimPositionManager expectAnimPositionManager = new ExpectAnimPositionManager(animExpectations, text, viewCalculator);
        expectAnimPositionManager.calculate();
        List<Animator>  list  =expectAnimPositionManager.getAnimators();
        assertTrue("Animators of value is not greater than zero", list .size()>0);

    }
}