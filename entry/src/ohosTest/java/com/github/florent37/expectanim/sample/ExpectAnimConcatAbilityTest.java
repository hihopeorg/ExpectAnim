package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.uitls.EventHelper;
import com.github.florent37.expectanim.sample.view.RoundImage;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExpectAnimConcatAbilityTest {
    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }
    @Test
    public void onStart() {
        ExpectAnimConcatAbility expectAnimConcatAbility = EventHelper.startAbility(ExpectAnimConcatAbility.class);
        sleep(2000);
        DirectionalLayout directionalLayout= (DirectionalLayout) expectAnimConcatAbility.findComponentById(ResourceTable.Id_content);
        RoundImage image1 = (RoundImage) expectAnimConcatAbility.findComponentById(ResourceTable.Id_image_1);
        float  startRotation=image1.getRotation();
        sleep(2000);
        EventHelper.triggerClickEvent(expectAnimConcatAbility, directionalLayout);
        sleep(3000);
        float  endRotation=image1.getRotation();
        assertTrue("Whether the head rotates ", startRotation!= endRotation);
    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}