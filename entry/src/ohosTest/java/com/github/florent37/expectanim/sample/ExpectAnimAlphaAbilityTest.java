package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.uitls.EventHelper;
import com.github.florent37.expectanim.sample.view.RoundImage;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExpectAnimAlphaAbilityTest {
    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }
    @Test
    public void onStart() {
        ExpectAnimAlphaAbility expectAnimAlphaAbility = EventHelper.startAbility(ExpectAnimAlphaAbility.class);
        Button button = (Button) expectAnimAlphaAbility.findComponentById(ResourceTable.Id_invisible);
        RoundImage image1= (RoundImage) expectAnimAlphaAbility.findComponentById(ResourceTable.Id_image_1);
        sleep(2000);
        EventHelper.triggerClickEvent(expectAnimAlphaAbility, button);
        sleep(2000);

        assertTrue("RoundImage is not ", image1.getVisibility()== Component.INVISIBLE);

    }

    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}