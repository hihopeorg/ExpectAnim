package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.core.custom.TextColorAnimExpectation;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class TextColorAnimExpectationTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getAnimator() {
        TextColorAnimExpectation textColorAnimExpectation =new TextColorAnimExpectation(0xff0faa);
        Animator animator = textColorAnimExpectation.getAnimator(new Text(sAbilityDelegator.getAppContext()));
        assertTrue("Animator of color is not null", animator!=null);

    }
}