package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignLeft;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignRight;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionAnimExpectationAlignRightTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getCalculatedValueX() {
        Text text=new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator=new ViewCalculator();
        PositionAnimExpectationAlignRight positionAnimExpectationAlignRight   =new  PositionAnimExpectationAlignRight(text);
        positionAnimExpectationAlignRight.setViewCalculator(viewCalculator);
        Float aFloat  =positionAnimExpectationAlignRight.getCalculatedValueX(text);
        assertTrue("getCalculatedValueX() of value is not null", aFloat!=null);
    }
}