package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationAlignTop;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationBelowOf;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionAnimExpectationBelowOfTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getCalculatedValueY() {

        Text text=new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator=new ViewCalculator();
        PositionAnimExpectationBelowOf positionAnimExpectationBelowOf   =new  PositionAnimExpectationBelowOf(text);
        positionAnimExpectationBelowOf.setViewCalculator(viewCalculator);
        Float aFloat  =positionAnimExpectationBelowOf.getCalculatedValueY(text);
        assertTrue("getCalculatedValueY() of value is not null", aFloat!=null);

    }

}