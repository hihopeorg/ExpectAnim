package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.Expectations;
import com.github.florent37.expectanim.core.alpha.ExpectAnimAlphaManager;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExpectAnimAlphaManagerTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Test
    public void getAnimators() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.alpha(0.5f));
        ViewCalculator viewCalculator = new ViewCalculator();
        Text text = new Text(sAbilityDelegator.getAppContext());
        ExpectAnimAlphaManager expectAnimAlphaManager = new ExpectAnimAlphaManager(animExpectations, text, viewCalculator);
        List<Animator> animators = expectAnimAlphaManager.getAnimators();

        assertTrue("ExpectAnimAlphaManager of calculate is not run", animators.size() > 0);
    }
}