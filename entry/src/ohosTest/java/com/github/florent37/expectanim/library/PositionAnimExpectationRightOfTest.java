package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.position.PositionAnimExpectationLeftOf;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionAnimExpectationRightOfTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void getCalculatedValueX() {
        Text text=new Text(sAbilityDelegator.getAppContext());
        PositionAnimExpectationLeftOf positionAnimExpectationLeftOf = new PositionAnimExpectationLeftOf(text);
        positionAnimExpectationLeftOf.setViewCalculator(new ViewCalculator());
        Float aFloat  =positionAnimExpectationLeftOf.getCalculatedValueX(text);
        assertTrue("getCalculatedValueX() of value is not null", aFloat!=null);


    }
}