package com.github.florent37.expectanim.library;

import com.github.florent37.expectanim.ViewCalculator;
import com.github.florent37.expectanim.core.AnimExpectation;
import com.github.florent37.expectanim.core.Expectations;
import com.github.florent37.expectanim.core.custom.CustomAnimExpectation;
import com.github.florent37.expectanim.core.custom.ExpectAnimCustomManager;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.components.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExpectAnimCustomManagerTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Test
    public void getAnimators() {
        List<AnimExpectation> animExpectations = new ArrayList<>();
        animExpectations.add(Expectations.toHaveBackgroundAlpha(1.0f));
        Text text = new Text(sAbilityDelegator.getAppContext());
        ViewCalculator viewCalculator = new ViewCalculator();
        ExpectAnimCustomManager expectAnimCustomManager = new ExpectAnimCustomManager(animExpectations, text, viewCalculator);
        expectAnimCustomManager.calculate();
        List<Animator> animators= expectAnimCustomManager.getAnimators();
        assertTrue("animators size is not added", animators.size() > 0);

    }
}