package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.uitls.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExpectAnimSampleAbilityTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    long longTime = 4000;
    long shortTime = 2000;
    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }


    @Test
    public void onStart() {

        Context context = sAbilityDelegator.getAppContext();
        ExpectAnimSampleAbility expectAnimSampleAbility = EventHelper.startAbility(ExpectAnimSampleAbility.class);
        sleep(shortTime);
        Text text = (Text) expectAnimSampleAbility.findComponentById(ResourceTable.Id_message);
        int[] startLocation=text.getLocationOnScreen();
        EventHelper.triggerClickEvent(expectAnimSampleAbility, text);
        sleep(longTime);
        int[] endLocation=text.getLocationOnScreen();
        assertTrue("Whether the starting and ending positions are equal", startLocation[0]!=endLocation[0]&&startLocation[1]!=endLocation[1] );
        sAbilityDelegator.stopAbility(expectAnimSampleAbility);

    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}