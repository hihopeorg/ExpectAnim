/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.florent37.expectanim.sample.uitl;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

public class IntentUtils {
    public static void startIntent(AbilitySlice context,Class<? extends Ability> intentClass) {
        Intent secondIntent = new Intent();
        // 指定待启动FA的bundleName和abilityName
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(context.getAbilityPackageContext().getBundleName())
                .withAbilityName(intentClass.getName())
                .build();
        secondIntent.setOperation(operation);
        context.startAbility(secondIntent); // 通过AbilitySlice的startAbility接口实现启动另一个页面
    }


}
