package com.github.florent37.expectanim.sample.slice;

import com.github.florent37.expectanim.sample.*;
import com.github.florent37.expectanim.sample.uitl.IntentUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_rotation).setClickedListener(component -> IntentUtils.startIntent(MainAbilitySlice.this, ExpectAnimRotationAbility.class));
        findComponentById(ResourceTable.Id_concat).setClickedListener(component -> IntentUtils.startIntent(MainAbilitySlice.this, ExpectAnimConcatAbility.class));
        findComponentById(ResourceTable.Id_flip).setClickedListener(component -> IntentUtils.startIntent(MainAbilitySlice.this, ExpectAnimFlipAbility.class));
        findComponentById(ResourceTable.Id_sample).setClickedListener(component -> IntentUtils.startIntent(MainAbilitySlice.this,ExpectAnimSampleAbility.class));
        findComponentById(ResourceTable.Id_visible).setClickedListener(component -> IntentUtils.startIntent(MainAbilitySlice.this,ExpectAnimAlphaAbility.class));
        findComponentById(ResourceTable.Id_setnow).setClickedListener(component -> IntentUtils.startIntent(MainAbilitySlice.this,ExpectAnimSetNowAbility.class));
        findComponentById(ResourceTable.Id_scroll).setClickedListener(component -> IntentUtils.startIntent(this,ExpectAnimScrollAbility.class));

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
