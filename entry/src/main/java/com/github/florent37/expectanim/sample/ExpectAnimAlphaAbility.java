package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.sample.slice.ExpectAnimAlphaAbilitySlice;
import com.github.florent37.expectanim.sample.view.RoundImage;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;

import static com.github.florent37.expectanim.core.Expectations.invisible;
import static com.github.florent37.expectanim.core.Expectations.visible;

public class ExpectAnimAlphaAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_alpha);

        RoundImage image1= (RoundImage) findComponentById(ResourceTable.Id_image_1);
        image1.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);



        image1.setClickedListener(component -> new ToastDialog(ExpectAnimAlphaAbility.this).setText("click").show());
        findComponentById(ResourceTable.Id_visible).setClickedListener(component -> new ExpectAnim()
                .expect(image1)
                .toBe(
                        visible()
                )
                .toAnimation()
                .setDuration(1000)
                .start());
        findComponentById(ResourceTable.Id_invisible).setClickedListener(component -> new ExpectAnim()
                .expect(image1)
                .toBe(
                        invisible()
                )
                .toAnimation()
                .setDuration(1000)
                .start());




    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
