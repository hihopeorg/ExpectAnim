package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.sample.slice.ExpectAnimConcatAbilitySlice;
import com.github.florent37.expectanim.sample.view.RoundImage;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import static com.github.florent37.expectanim.core.Expectations.*;

public class ExpectAnimConcatAbility extends Ability {
    private ExpectAnim expectAnimMove;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_concat);
        findComponentById(ResourceTable.Id_content).setClickedListener(component -> {
            expectAnimMove.setPercent(0f);
            expectAnimMove.start();


        });
        RoundImage image1 = (RoundImage) findComponentById(ResourceTable.Id_image_1);
        RoundImage image2 = (RoundImage) findComponentById(ResourceTable.Id_image_2);
        RoundImage image3 = (RoundImage) findComponentById(ResourceTable.Id_image_3);
        RoundImage image4 = (RoundImage) findComponentById(ResourceTable.Id_image_4);

        image1.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);
        image2.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);
        image3.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);
        image4.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);


        this.expectAnimMove = ExpectAnim.concat(
                new ExpectAnim()
                        .expect(image1)
                        .toBe(
                                withCameraDistance(500f),
                                flippedHorizontally()
                        )
                        .toAnimation()
                        .setDuration(1500),
                new ExpectAnim()
                        .expect(image2)
                        .toBe(
                                withCameraDistance(1000f),
                                flippedVertically()
                        )
                        .toAnimation()
                        .setDuration(500),
                new ExpectAnim()
                        .expect(image3)
                        .toBe(
                                withCameraDistance(1500f),
                                flippedVertically()
                        )
                        .toAnimation()
                        .setDuration(300),
                new ExpectAnim()
                        .expect(image4)
                        .toBe(
                                withCameraDistance(2000f),
                                flippedHorizontallyAndVertically()
                        )
                        .toAnimation()
                        .setDuration(1500)
        )
                .start();

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
