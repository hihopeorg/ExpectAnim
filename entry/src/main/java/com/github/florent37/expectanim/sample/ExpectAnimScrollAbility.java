package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.sample.slice.ExpectAnimScrollAbilitySlice;
import com.github.florent37.expectanim.sample.uitl.Log;
import com.github.florent37.expectanim.sample.view.RoundImage;
import com.github.florent37.expectanim.uitls.Gravity;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.NestedScrollView;
import ohos.agp.components.Text;

import static com.github.florent37.expectanim.core.Expectations.*;

public class ExpectAnimScrollAbility extends Ability {

    private ExpectAnim expectAnimMove;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_scroll);

        RoundImage avatar = (RoundImage) findComponentById(ResourceTable.Id_avatar);
        avatar.setTag("头像");
        avatar.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);

        Text username = (Text) findComponentById(ResourceTable.Id_username);
        username.setTag("username");
        Text follow = (Text) findComponentById(ResourceTable.Id_follow);
        username.setTag("follow");
        Component backbground = findComponentById(ResourceTable.Id_background);
        NestedScrollView scrollView = (NestedScrollView) findComponentById(ResourceTable.Id_scrollview);

        this.expectAnimMove = new ExpectAnim()
                .expect(avatar)
                .toBe(
                        topOfParent().withMarginDp(20),
                        leftOfParent().withMarginDp(20),
                        scale(0.5f, 0.5f)
                )

                .expect(username)
                .toBe(
                        toRightOf(avatar).withMarginDp(16),
                        sameCenterVerticalAs(avatar),
                        alpha(0.5f)
                )

                .expect(follow)
                .toBe(
                        rightOfParent().withMarginDp(20),
                        sameCenterVerticalAs(avatar)
                )

                .expect(backbground)
                .toBe(
                        height(290).withGravity(Gravity.LEFT, Gravity.TOP)
                )

                .toAnimation();

//        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//
//            }
//        });

        scrollView.setScrolledListener((component, scrollX, scrollY, oldScrollX, oldScrollY) -> {

//                final float percent = (scrollY * 1f) /100*scrollView.getEstimatedHeight();
            final float percent = (scrollY * 1f) / component.getHeight();

            if(Math.abs(scrollY)>190){
                expectAnimMove.setPercent(1,false);
            }else {
                expectAnimMove.setPercent(0,true);

            }
            Log.d("TAG", "scrollY===" + scrollY + "----------percent==" + percent+"----oldScrollY-"+oldScrollY);
        });


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
