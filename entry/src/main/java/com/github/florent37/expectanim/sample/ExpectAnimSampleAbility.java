package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.sample.slice.ExpectAnimSampleAbilitySlice;
import com.github.florent37.expectanim.sample.view.RoundImage;
import com.github.florent37.expectanim.uitls.Gravity;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import static com.github.florent37.expectanim.core.Expectations.*;

public class ExpectAnimSampleAbility extends Ability {
    private ExpectAnim expectAnimMove;
    private ExpectAnim followAnimMove;
    private StackLayout bottomLayout;
    private boolean isClick = false;
    private StackLayout content;
    private RoundImage avatar;
    private Text name;
    private Text subname;
    private Text follow;
    private Text message;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_sample);
        bottomLayout = (StackLayout) findComponentById(ResourceTable.Id_bottomLayout);
        content = (StackLayout) findComponentById(ResourceTable.Id_content);
        avatar = (RoundImage) findComponentById(ResourceTable.Id_avatar);
        avatar.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);
        name = (Text) findComponentById(ResourceTable.Id_name);
        subname = (Text) findComponentById(ResourceTable.Id_subname);
        follow = (Text) findComponentById(ResourceTable.Id_follow);
        message = (Text) findComponentById(ResourceTable.Id_message);
        message.setClickedListener(component -> {
            isClick = true;
            expectAnimMove.start();
        });

        follow.setClickedListener(component -> {
                    initFollow();
                }
        );
        intBottom();


        new ExpectAnim()
                .expect(bottomLayout)
                .toBe(
                        outOfScreen(Gravity.BOTTOM)
                )
                .expect(content)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )
                .toAnimation()
                .setNow();

        this.expectAnimMove = new ExpectAnim()

                .expect(avatar)
                .toBe(
                        bottomOfParent().withMarginDp(36),
                        leftOfParent().withMarginDp(16),
                        width(40).toDp().keepRatio()
                )

                .expect(name)
                .toBe(
                        toRightOf(avatar).withMarginDp(16),
                        sameCenterVerticalAs(avatar),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(subname)
                .toBe(
                        toRightOf(name).withMarginDp(5),
                        sameCenterVerticalAs(name),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(follow)
                .toBe(
                        rightOfParent().withMarginDp(4),
                        bottomOfParent().withMarginDp(12),
                        toHaveBackgroundAlpha(1f)
                )

                .expect(message)
                .toBe(
                        aboveOf(follow).withMarginDp(4),
                        rightOfParent().withMarginDp(4),
                        toHaveBackgroundAlpha(1f)
                )

                .expect(bottomLayout)
                .toBe(
                        atItsOriginalPosition()
                )
                .expect(content)
                .toBe(
                        atItsOriginalPosition(),
                        visible()
                )

                .toAnimation()
                .setDuration(1500);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void intBottom() {
        getMainTaskDispatcher().delayDispatch(() -> bottomLayout.setTranslationY(bottomLayout.getTranslationY() + bottomLayout.getEstimatedHeight()), 100);

    }


    public void initFollow() {
        if (!isClick) {
            new ExpectAnim()

                    .expect(avatar)
                    .toBe(
                            bottomOfParent().withMarginDp(36),
                            leftOfParent().withMarginDp(16),
                            width(40).toDp().keepRatio()
                    )

                    .expect(name)
                    .toBe(
                            toRightOf(avatar).withMarginDp(16),
                            sameCenterVerticalAs(avatar),
                            toHaveTextColor(Color.getIntColor("#ffffff"))
                    )

                    .expect(subname)
                    .toBe(
                            toRightOf(name).withMarginDp(5),
                            sameCenterVerticalAs(name),
                            toHaveTextColor(Color.getIntColor("#ffffff"))
                    )

                    .expect(follow)
                    .toBe(
                            rightOfParent().withMarginDp(4),
                            bottomOfParent().withMarginDp(12),
                            toHaveBackgroundAlpha(1f)
                    )

                    .expect(message)
                    .toBe(
                            aboveOf(follow).withMarginDp(4),
                            rightOfParent().withMarginDp(4),
                            toHaveBackgroundAlpha(1f)
                    )

                    .expect(bottomLayout)
                    .toBe(
                            atItsOriginalPosition()
                    )
                    .expect(content)
                    .toBe(
                            atItsOriginalPosition(),
                            visible()
                    ).toAnimation().setDuration(300).start();
        }

        EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                followAnim();
                if (followAnimMove != null) {
                    followAnimMove.start();
                }
                isClick = false;
            }
        }, 500);


    }

    private void followAnim() {
        this.followAnimMove = new ExpectAnim()
                .expect(avatar)
                .toBe(
                        centerInParent(true, true).withMarginDp(0),
                        width(80).toDp().keepRatio(),
                        height(80).toDp().keepRatio()
                )

                .expect(name)
                .toBe(
                        belowOf(avatar).withMarginDp(12),
                        centerHorizontalInParent(),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(subname)
                .toBe(
                        belowOf(name).withMarginDp(6),
                        centerHorizontalInParent()
                )

                .expect(follow)
                .toBe(
                        leftOfParent().withMarginDp(38),
                        sameCenterVerticalAs(avatar)

                )

                .expect(message)
                .toBe(
                        rightOfParent().withMarginDp(20),
                        sameCenterVerticalAs(avatar)
                )

                .expect(bottomLayout)
                .toBe(
                        outOfScreen(Gravity.BOTTOM)
                )
                .expect(content)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )

                .toAnimation()
                .setDuration(1500);
    }
}
