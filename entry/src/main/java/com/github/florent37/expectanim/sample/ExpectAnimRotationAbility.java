package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.sample.slice.ExpectAnimRotationAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

import static com.github.florent37.expectanim.core.Expectations.*;

public class ExpectAnimRotationAbility extends Ability {
    private ExpectAnim expectAnimMove;

    private Text text1;
    private Text text3;
    private Text text4;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_rotation);

        text1 = (Text) findComponentById(ResourceTable.Id_text1);
        Text text2 = (Text) findComponentById(ResourceTable.Id_text2);
        text3 = (Text) findComponentById(ResourceTable.Id_text3);
        text4 = (Text) findComponentById(ResourceTable.Id_text4);


        this.expectAnimMove = new ExpectAnim()

                .expect(text1)
                .toBe(
                        topOfParent(),
                        leftOfParent(),
                        rotated(90)
                )

                .expect(text2)
                .toBe(
                        alignLeft(text1),
                        belowOf(text1)
                )

                .expect(text3)
                .toBe(
                        alignTop(text1),
                        toRightOf(text1)
                )

                .expect(text4)
                .toBe(
                        belowOf(text3),
                        alignLeft(text3)
                )

                .toAnimation()
                .setDuration(1500);
        findComponentById(ResourceTable.Id_content).setClickedListener(component -> {
            if (text1.getRotation() == 0) {
                expectAnimMove.start();
            } else {
                new ExpectAnim()

                        .expect(text1)
                        .toBe(
                                centerInParent(true, true),
                                rotated(0)
                        )

                        .expect(text2)
                        .toBe(
                                belowOf(text1),
                                centerHorizontalInParent()
                        )

                        .expect(text3)
                        .toBe(
                                belowOf(text2),
                                centerHorizontalInParent()
                        )

                        .expect(text4)
                        .toBe(
                                belowOf(text3).withMarginDp(40),
                                centerHorizontalInParent()
                        )

                        .toAnimation()
                        .setDuration(1500).start();


            }

        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
