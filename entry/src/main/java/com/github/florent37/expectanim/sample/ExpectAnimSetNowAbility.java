package com.github.florent37.expectanim.sample;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.sample.slice.ExpectAnimSetNowAbilitySlice;
import com.github.florent37.expectanim.sample.view.RoundImage;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import static com.github.florent37.expectanim.core.Expectations.invisible;

public class ExpectAnimSetNowAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_set_now);
        RoundImage avatar = (RoundImage) findComponentById(ResourceTable.Id_avatar);
        avatar.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);
        new ExpectAnim()
                .expect(avatar)
                .toBe(
                        invisible()
                )
                .toAnimation()
                .setNow();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
