package com.github.florent37.expectanim.sample.slice;

import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.uitls.Gravity;
import com.github.florent37.expectanim.sample.ResourceTable;
import com.github.florent37.expectanim.sample.view.RoundImage;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import static com.github.florent37.expectanim.core.Expectations.*;

public class ExpectAnimSampleAbilitySlice extends AbilitySlice {
    private ExpectAnim expectAnimMove;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_expect_anim_sample);
        StackLayout bottomLayout  = (StackLayout) findComponentById(ResourceTable.Id_bottomLayout);
        StackLayout  content= (StackLayout) findComponentById(ResourceTable.Id_content);
        RoundImage avatar= (RoundImage) findComponentById(ResourceTable.Id_avatar);
        avatar.setPixelMapAndCircle(ResourceTable.Media_expectanim_small);
        Text name= (Text) findComponentById(ResourceTable.Id_name);
        Text subname= (Text) findComponentById(ResourceTable.Id_subname);
        Text follow= (Text) findComponentById(ResourceTable.Id_follow);
        Text message= (Text) findComponentById(ResourceTable.Id_message);
         message.setClickedListener(component -> expectAnimMove.start());

        follow.setClickedListener(component -> expectAnimMove.reset());


        new ExpectAnim()
                .expect(bottomLayout)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )
                .expect(content)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )
                .toAnimation()
                .setNow();

        this.expectAnimMove = new ExpectAnim()

                .expect(avatar)
                .toBe(
                        bottomOfParent().withMarginDp(36),
                        leftOfParent().withMarginDp(16),
                        width(40).toDp().keepRatio()
                )

                .expect(name)
                .toBe(
                        toRightOf(avatar).withMarginDp(16),
                        sameCenterVerticalAs(avatar),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(subname)
                .toBe(
                        toRightOf(name).withMarginDp(5),
                        sameCenterVerticalAs(name),
                        toHaveTextColor(Color.getIntColor("#ffffff"))
                )

                .expect(follow)
                .toBe(
                        rightOfParent().withMarginDp(4),
                        bottomOfParent().withMarginDp(12),
                        toHaveBackgroundAlpha(1f)
                )

                .expect(message)
                .toBe(
                        aboveOf(follow).withMarginDp(4),
                        rightOfParent().withMarginDp(4),
                        toHaveBackgroundAlpha(1f)
                )

                .expect(bottomLayout)
                .toBe(
                        atItsOriginalPosition(),
                        visible()
                )

                .expect(content)
                .toBe(
                        atItsOriginalPosition(),
                        visible()
                )

                .toAnimation()
                .setDuration(1500);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
